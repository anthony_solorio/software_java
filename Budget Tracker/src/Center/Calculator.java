package Center;

import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import Deductions.*;
import Income.*;
import StartingLine.Base;



public class Calculator { 
	
	public static void createNewTable() {
        // SQLite connection string
        String url = "http://SoftwareJava_project-anthonygarzasolorio247937.codeanyapp.com";
        
        // SQL statement for creating a new table
        String sql = "CREATE TABLE IF NOT EXISTS StuData (\n"
                + "	name varchar(20),\n"
                + "	clothing float,\n"
                + "	dorm float, \n"
                + "	entertainment float, \n"
                + "	miscMonthD float, \n"
                + "	miscOnceI float, \n"
                + "	rent float, \n"
                + "	supply float, \n"
                + "	textboook float, \n"
                + "	transportation float, \n"
                + "	tuition float, \n"
                + "	utility float, \n"
                + "	totalD float, \n"
                + "	grant float, \n"
                + "	job float, \n"
                + "	loan float, \n"
                + "	miscMonthI float, \n"
                + "	miscOnceI float, \n"
                + "	totalI float, \n"
                + "	base float, \n"
                + ");";
        
        try (Connection conn = DriverManager.getConnection(url);
                Statement stmt = conn.createStatement()) {
            // create a new table
            stmt.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
	
	public static void main(String args[]) throws IOException{
		//used to set all values
		float setter = 0;
		String fullName = new String();
		String pick = new String();
		
		//all deduction cost
		ClothingDeduction cd = new ClothingDeduction();
		EntertainmentDeduction ed = new EntertainmentDeduction();
		MiscMonthlyDeduction mmd = new MiscMonthlyDeduction();
		MiscOnceDeduction mod = new MiscOnceDeduction();
		RentDeduction rd = new RentDeduction();
		DormDeduction dd = new DormDeduction();
		SupplyDeduction sd = new SupplyDeduction();
		TextbookDeduction tbd = new TextbookDeduction();
		TransportationDeduction td = new TransportationDeduction();
		TuitionDeduction tud = new TuitionDeduction();
		UtilityDeduction ud = new UtilityDeduction();
		
		//all sources of income
		GrantIncome gi = new GrantIncome();
		JobIncome ji = new JobIncome();
		LoanIncome li = new LoanIncome();
		MiscMonthlyIncome mmi = new MiscMonthlyIncome();
		MiscOnceIncome moi = new MiscOnceIncome();
		
		//totals
		TotalIncome ti = new TotalIncome();
		TotalDeduction tod = new TotalDeduction();
		
		//Starting money
		Base b = new Base();
		
		//creates table in database if does not exists 
		createNewTable();
		
		//introduction
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Welcome to the college student budget calculator!");
		System.out.println("I'm going to ask a few questions to help calculate your monthly budget.");
		System.out.println("Let's get started!");
		
		System.out.println();
		
		System.out.println("What is your name?");
		fullName = reader.readLine();
		//Ask the user what they would like to do
		do {
			System.out.println("1: Are you here for the first time?");
			System.out.println("2. Updating your information?");
			System.out.println("3. Or would you like to see your information?");
			System.out.println("Enter 1, 2, or 3");
			pick = reader.readLine();
			//can only pick 1 2 or 3
			if(pick != "1" || pick != "2" || pick != "3") {
				
			}
		}while(pick != "1" || pick != "2" || pick != "3");
		//if use picks one they are new, ask them all the questions to help calculate everything
		if(pick == "1") {
			//ask for a base line of money to get started
			System.out.println("1: How much money are you starting out with?");
			//saves that figure
			do {
				String s = new String();
				s = reader.readLine();
				setter = Float.parseFloat(s);
				if(setter < 0) {
					System.out.println("The lowest ammount of money you can start with is 0");
					System.out.println();
				}
			}while(setter < 0);
			b.setStartingIncome(setter);
			setter = 0;
			
			//asks for grant and scholarship money
			System.out.println("2: Let move on to income.");
			System.out.println("If you have any total the amount of money recieved in grants and scholarships.");
			System.out.println("If no money is recieved enter 0.");
			//save information
			do {
				String s = new String();
				s = reader.readLine();
				setter = Float.parseFloat(s);
				if(setter < 0) {
					System.out.println("You cannot have negative grants.");
				}
			}while(setter < 0);
			gi.setIncome(setter);
			setter = 0;
			//asks for monthly job income
			System.out.println("3. If you have a job enter in your estimate earnings for this month.");
			System.out.println("Something that could be helpful is to estimate lower than what you are actually expecting.");
			System.out.println("If you do not have a job enter 0.");
			//saves information
			do {
				String s = new String();
				s = reader.readLine();
				setter = Float.parseFloat(s);
				if(setter < 0) {
					System.out.println("You cannot earn negative income from a job.");
				}
			}while(setter < 0);
			ji.setIncome(setter);
			setter = 0;
			
			//asks for money incoming from loans
			System.out.println("4. Next if you have any loans enter the amount recieved.");
			System.out.println("For this we are assuming that you do not have to start paying off loans until after college.");
			System.out.println("If you are paying off loans we will take care of that in a later section.");
			System.out.println("If you do not have any loans enter 0.");
			//saves information
			do {
				String s = new String();
				s = reader.readLine();
				setter = Float.parseFloat(s);
				if(setter < 0) {
					System.out.println("You cannot have negative loan(s).");
				}
			}while(setter < 0);
			li.setIncome(setter);
			setter = 0;
			
			//asks for other sources of monthly income
			System.out.println("5. If you have another source of income you recieve monthly add that here.");
			System.out.println("If you already added that with the Job income enter 0.");
			System.out.println("Like the job income it is helpful to under estimate your guess.");
			//saves them here
			do {
				String s = new String();
				s = reader.readLine();
				setter = Float.parseFloat(s);
				if(setter < 0) {
					System.out.println("You cannot earn negative income.");
				}
			}while(setter < 0);
			mmi.setIncome(setter);
			setter = 0;
			
			//asks for for income that is not recieved monthly
			System.out.println("6. Lastly enter any money that you do not recieve consitantly.");
			System.out.println("This can range anywhere from birthday money to money recieved for helping someone.");
			System.out.println("If you are expecting money, but have not recieved it, estimate your earnings.");
			//saves information
			do {
				String s = new String();
				s = reader.readLine();
				setter = Float.parseFloat(s);
				if(setter < 0) {
					System.out.println("You cannot earn negative income.");
				}
			}while(setter < 0);
			moi.setIncome(setter);
			setter = 0;
			
			//calculates the total income for this month
			setter = gi.getIncome() + ji.getIncome() + li.getIncome() + mmi.getIncome() + moi.getIncome();
			ti.setTotal(setter);
			setter= 0;
			
			//Moving on to money they need to pay this month
			System.out.println("Great! We'll calculate your total and get back to you at the end.");
			System.out.println("Now comes that not so fun part...");
			
			System.out.println();
			//asks for money spent on clothing
			System.out.println("1. Enter the amount you have spent or plan to spend on clothing.");
			System.out.println("When estimating the amount spending estimate higher than you think.");
			//saves information
			do {
				String s = new String();
				s = reader.readLine();
				setter = Float.parseFloat(s);
				if(setter < 0) {
					System.out.println("Do not enter in a negative value. We will calculate it as though it was negative later.");
				}
			}while(setter < 0);
			cd.setDeduct(setter);
			setter = 0;
			
			//asks for money spent on themselves 
			System.out.println("2. Enter the amount of money you expect to spend on entertainment.");
			System.out.println("This will inlude stuff like going to see a movie, or buying videogames.");
			System.out.println("This will be money specifically set aside for you to have fun.");
			System.out.println("This means the amount you enter will be used to calculate monthly exxpenses.");
			//saves it here
			do {
				String s = new String();
				s = reader.readLine();
				setter = Float.parseFloat(s);
				if(setter < 0) {
					System.out.println("Do not enter in a negative value. We will calculate it as though it was negative later.");
				}
			}while(setter < 0);
			ed.setDeduct(setter);
			setter = 0;
			
			//Asks for money used on rent
			System.out.println("3. If you are leasing an apartment, enter your rent for the month.");
			System.out.println("If you pay for your own utilties do not add them here.");
			System.out.println("There will be a section for utilities later.");
			System.out.println("If you live in a dorm enter 0.");
			//saves information
			do {
				String s = new String();
				s = reader.readLine();
				setter = Float.parseFloat(s);
				if(setter < 0) {
					System.out.println("Do not enter in a negative value. We will calculate it as though it was negative later.");
				}
			}while(setter < 0);
			rd.setDeduct(setter);
			setter = 0;
			//asks for money used 
			System.out.println("4. If you live in the dorms enter your yearly dorm cost.");
			System.out.println("We will calculate the cost as though you were paying off the dorm monthly.");
			//saves it here
			do {
				String s = new String();
				s = reader.readLine();
				setter = Float.parseFloat(s);
				if(setter < 0) {
					System.out.println("Do not enter in a negative value. We will calculate it as though it was negative later.");
				}
			}while(setter < 0);
			dd.setDeduct(setter/5);
			setter = 0;
			
			//asks for money used on school supplies
			System.out.println("5. Now please enter the amount of money you plan to spend on school supplies.");
			System.out.println("This will be an expense that will only calculate once and not every month.");
			//saves infromation
			do {
				String s = new String();
				s = reader.readLine();
				setter = Float.parseFloat(s);
				if(setter < 0) {
					System.out.println("Do not enter in a negative value. We will calculate it as though it was negative later.");
				}
			}while(setter < 0);
			sd.setDeduct(setter);
			setter = 0;
			
			//asks for textbook/book cost
			System.out.println("6. Next will be the cost of textbooks.");
			System.out.println("Like the previous entry this will only be used to calculate once.");
			System.out.println("Cost of regual books can also be put here.");
			System.out.println("Just be sure you did not also put the cost in the entertainment deductions");
			//saves information
			do {
				String s = new String();
				s = reader.readLine();
				setter = Float.parseFloat(s);
				if(setter < 0) {
					System.out.println("Do not enter in a negative value. We will calculate it as though it was negative later.");
				}
			}while(setter < 0);
			tbd.setDeduct(setter);
			setter = 0;
			
			//asks for money used on transportation
			System.out.println("7. Enter in the amount of money used for transportation.");
			System.out.println("This can range anywhere from the cost of public transportation to gas for your vehical.");
			System.out.println("This will calculate monthly so be sure not to add vehical repairs.");
			//saves information
			do {
				String s = new String();
				s = reader.readLine();
				setter = Float.parseFloat(s);
				if(setter < 0) {
					System.out.println("Do not enter in a negative value. We will calculate it as though it was negative later.");
				}
			}while(setter < 0);
			td.setDeduct(setter);
			setter = 0;
			//asks for all utilities 
			System.out.println("8. Now we are moving to the more heavy hitting deductions, utilities.");
			System.out.println("If for any of the following utilities you do not have to pay them enter in a zero.");
			System.out.println("Otherwise enter in the monthly cost of the utility specified.");
			System.out.println("If the utility is not a fixed price estimate as best you can.");
			System.out.println("Make sure to estimate high.");
			System.out.println();
			//asks for cable  fee
			System.out.println("8.1. Okay lets get started. Enter in your cable fees.");
			//saves cable fee
			do {
				String s = new String();
				s = reader.readLine();
				setter = Float.parseFloat(s);
				if(setter < 0) {
					System.out.println("Do not enter in a negative value. We will calculate it as though it was negative later.");
				}
			}while(setter < 0);
			ud.setCable(setter);
			setter = 0;
			//asks for electricity fee
			System.out.println("8.2. Now enter your electricity fees.");
			//saves information
			do {
				String s = new String();
				s = reader.readLine();
				setter = Float.parseFloat(s);
				if(setter < 0) {
					System.out.println("Do not enter in a negative value. We will calculate it as though it was negative later.");
				}
			}while(setter < 0);
			ud.setElectricity(setter);
			setter = 0;
			//asks for gas fee
			System.out.println("8.3. Enter your gas fees.");
			//saves information
			do {
				String s = new String();
				s = reader.readLine();
				setter = Float.parseFloat(s);
				if(setter < 0) {
					System.out.println("Do not enter in a negative value. We will calculate it as though it was negative later.");
				}
			}while(setter < 0);
			ud.setGas(setter);
			setter = 0;
			//asks for Internet fee
			System.out.println("8.4. Enter your internet fees.");
			//saves information
			do {
				String s = new String();
				s = reader.readLine();
				setter = Float.parseFloat(s);
				if(setter < 0) {
					System.out.println("Do not enter in a negative value. We will calculate it as though it was negative later.");
				}
			}while(setter < 0);
			ud.setInternet(setter);
			setter = 0;
			//asks for phone fee
			System.out.println("8.5. Enter your phone fees.");
			//saves it here
			do {
				String s = new String();
				s = reader.readLine();
				setter = Float.parseFloat(s);
				if(setter < 0) {
					System.out.println("Do not enter in a negative value. We will calculate it as though it was negative later.");
				}
			}while(setter < 0);
			ud.setPhone(setter);
			setter = 0;
			//asks for water fee
			System.out.println("8.6. Enter your water fees.");
			//saves inforation 
			do {
				String s = new String();
				s = reader.readLine();
				setter = Float.parseFloat(s);
				if(setter < 0) {
					System.out.println("Do not enter in a negative value. We will calculate it as though it was negative later.");
				}
			}while(setter < 0);
			ud.setWater(setter);
			setter = 0;
			
			System.out.println("Awesome! We will calculate utility costs now.");
			//calculates total cost of all utilites
			ud.calculateTotalUtility();
			
			//asks for monthly tuition cost
			System.out.println("9. Next will be the cost of tuition.");
			System.out.println("Enter in the full monthly cost of tution.");
			System.out.println("Do not calculate tution with scholarships or grants.");
			//saves information
			do {
				String s = new String();
				s = reader.readLine();
				setter = Float.parseFloat(s);
				if(setter < 0) {
					System.out.println("Do not enter in a negative value. We will calculate it as though it was negative later.");
				}
			}while(setter < 0);
			tud.setDeduct(setter);
			setter = 0;
			//asks for anything we may have forgotten to include
			System.out.println("10. Enter in any deductions that were not mentioned above.");
			System.out.println("This deduction will happen monthly.");
			//saves information
			do {
				String s = new String();
				s = reader.readLine();
				setter = Float.parseFloat(s);
				if(setter < 0) {
					System.out.println("Do not enter in a negative value. We will calculate it as though it was negative later.");
				}
			}while(setter < 0);
			mmd.setDeduct(setter);
			setter = 0;
			//asks for anything we forgot to include 
			System.out.println("11. Lastly, enter in any deduction that were not mentioned above");
			System.out.println("This deduction will only happen once.");
			//saves information
			do {
				String s = new String();
				s = reader.readLine();
				setter = Float.parseFloat(s);
				if(setter < 0) {
					System.out.println("Do not enter in a negative value. We will calculate it as though it was negative later.");
				}
			}while(setter < 0);
			mod.setDeduct(setter);
			setter = 0;
			
			System.out.println("YOU'RE DONE!");
			System.out.println("Your information is being processed");
			
			// SQLite connection string
	        String url = "http://SoftwareJava_project-anthonygarzasolorio247937.codeanyapp.com";
	        
	        // SQL statement for creating a new table
	        String sql = "INSERT INTO StuData (\n"
	                + 	fullName + ",\n"
	                + 	cd.getDeduct() + ",\n"
	                + 	dd.getDeduct() + ", \n"
	                +   ed.getDeduct() + ", \n"
	                + 	mmd.getDeduct() + ", \n"
	                + 	mod.getDeduct() + ", \n"
	                + 	rd.getDeduct() + ", \n"
	                + 	sd.getDeduct() + ", \n"
	                + 	tbd.getDeduct() + ", \n"
	                + 	td.getDeduct() + ", \n"
	                + 	tud.getDeduct() + ", \n"
	                + 	ud.getTotal() + ", \n"
	                + 	tod.getDeduct() + ", \n"
	                + 	gi.getIncome() + ", \n"
	                + 	ji.getIncome() + ", \n"
	                + 	li.getIncome() + ", \n"
	                + 	mmi.getIncome() + ", \n"
	                + 	moi.getIncome() + ", \n"
	                + 	ti.getTotal() + ", \n"
	                + 	b.getStartingIncome() + ", \n"
	                + ");";
	        
	        try (Connection conn = DriverManager.getConnection(url);
	                Statement stmt = conn.createStatement()) {
	            // create a new table
	            stmt.execute(sql);
	        } catch (SQLException e) {
	            System.out.println(e.getMessage());
	        }
			
			//calculates total deduction
			tod.setDeduct(cd.getDeduct() + dd.getDeduct() + ed.getDeduct() + mmd.getDeduct() + mod.getDeduct() + rd.getDeduct() + sd.getDeduct()
			+ tbd.getDeduct() + td.getDeduct() + tud.getDeduct() + ud.getTotal());
			
			//outputs to the screen all the information to the screen
			System.out.println("Here is all your information.");
			System.out.println("You came in with " + b.getStartingIncome());
			System.out.println("This month you will recieve " + ti.getTotal());
			System.out.println("In total you will have about " + (b.getStartingIncome() + ti.getTotal()));
			System.out.println("This month you will need to pay about " + tod.getDeduct());
			System.out.println("Taking the differnce you will have " + ((b.getStartingIncome() + ti.getTotal()) - tod.getDeduct()));
		}
		//if 2 picked user can update one of the following catagories
		else if(pick == "2") {
			System.out.println("Select one of the following to update");
			System.out.println("1. Clothing Deduction");
			System.out.println("2. Dorm Deduction");
			System.out.println("3. Entertainment Deduction");
			System.out.println("4. Misc. Monthly Deduction");
			System.out.println("5. Misc. Once Deduction");
			System.out.println("6. Rent Deduction");
			System.out.println("7. Supply Deduction");
			System.out.println("8. Textbook Deduction");
			System.out.println("9. Transportation Deduction");
			System.out.println("10. Tuition Deduction");
			System.out.println("11. Utility Deduction");
			System.out.println("12. Grant Income");
			System.out.println("13. Job Income");
			System.out.println("14. Loan Income");
			System.out.println("15. Misc. Monthly Income");
			System.out.println("16. Misc. Once Income");
			System.out.println("17. Base Money");
			//this will keep prompting the user until they pick a number 1 - 17
			do {
				pick = reader.readLine();
				if(pick != "1" || pick != "2" || 
				   pick != "3" || pick != "4" ||
				   pick != "5" || pick != "6" ||
				   pick != "7" || pick != "8" ||
				   pick != "9" || pick != "10" ||
				   pick != "11" || pick != "12" ||
				   pick != "13" || pick != "14" ||
				   pick != "15" || pick != "16" || pick != "17") {
					System.out.println("Must pick a number 1 - 17");
				}
			}while(pick != "1" || pick != "2" || 
				   pick != "3" || pick != "4" ||
				   pick != "5" || pick != "6" ||
				   pick != "7" || pick != "8" ||
				   pick != "9" || pick != "10" ||
				   pick != "11" || pick != "12" ||
				   pick != "13" || pick != "14" ||
				   pick != "15" || pick != "16" || pick != "17");
			String update = new String();
			//11 is a specical case since it has multiple parts
			//That is why it is here
			if(pick == "11") {
				update = "utility";
				//instead of picking one they must update all
				//This is to make it easier when updating utilities multiple times
				System.out.println("Enter in amounts for ALL utilities");
				System.out.println("1. cable");
				System.out.println("2. electricity");
				System.out.println("3. gas");
				System.out.println("4. internet");
				System.out.println("5. phone");
				System.out.println("6. water");
				int i = 1;
				
				//as long as i is less than 7 then the loop continutes
				do {
					String s = new String();
					System.out.println(i);
					s = reader.readLine();
					setter = Float.parseFloat(s);
					if(setter < 0) {
						System.out.println("Do not enter in a negative value.");
					}
					//i will only be updated if the user enters a positive value
					//If a positive value is entered the cost is set and i is incremented
					if(setter >= 0) {
						if(i == 1) {
							ud.setCable(setter);
						}
						else if(i == 2) {
							ud.setElectricity(setter);
						}
						else if(i == 3) {
							ud.setGas(setter);
						}
						else if(i == 4) {
							ud.setInternet(setter);
						}
						else if(i == 5) {
							ud.setPhone(setter);
						}
						else if(i == 6) {
							ud.setWater(setter);
						}
						i++;
					}
				}while(i < 7);
				//the new total is calculated
				ud.calculateTotalUtility();
				//setter is set to be used later
				setter = ud.getTotal();
			}
			//if utilities are not picked
			else if(pick != "11") {
				//the user will first enter in a new amount for the category they picked
				System.out.println("Enter in new amount.");
				//they will keep being prompted until cost is not negative
				do {
					String s = new String();
					s = reader.readLine();
					setter = Float.parseFloat(s);
					if(setter < 0) {
						System.out.println("Do not enter in a negative value.");
					}
				}while(setter < 0);
				
				//setter is set to be used later 
				//the category is also set to be used later
				if(pick == "1") {
					update = "clothing";
				}
				else if(pick == "2") {
					update = "dorm";
				}
				else if(pick == "3") {
					update = "entertainment";
				}
				else if(pick == "4") {
					update = "miscMonthD";
				}
				else if(pick == "5") {
					update = "miscOnceD";
				}
				else if(pick == "6") {
					update = "rent";
				}
				else if(pick == "7") {
					update = "supply";
				}
				else if(pick == "8") {
					update = "textbook";
				}
				else if(pick == "9") {
					update = "transportation";
				}
				else if(pick == "10") {
					update = "tuition";
				}
				else if(pick == "12") {
					update = "grant";
				}
				else if(pick == "13") {
					update = "job";
				}
				else if(pick == "14") {
					update = "loan";
				}
				else if(pick == "15") {
					update = "miscMonthI";
				}
				else if(pick == "16") {
					update = "misconceI";
				}
				else if(pick == "17") {
					update = "base";
				}
			}
			// SQLite connection string
	        String url = "http://SoftwareJava_project-anthonygarzasolorio247937.codeanyapp.com";
	        
	        // SQL statement for updating
	        String sql = "UPDATE StuData (\n"
	        		+ "SET" + update + "= '"+setter + "' WHERE name = " + fullName + ")";
	        
	        try (Connection conn = DriverManager.getConnection(url);
	                Statement stmt = conn.createStatement()) {
	            // create a new table
	            stmt.execute(sql);
	        } catch (SQLException e) {
	            System.out.println(e.getMessage());
	        }
		}
		//if the user would just like to view their information then the information associated with their name is executed
		else if(pick == "3") {
			// SQLite connection string
	        String url = "http://SoftwareJava_project-anthonygarzasolorio247937.codeanyapp.com";
	        
	        // SQL statement for creating a new table
	        String sql = "SELECT * FROM StuData WHERE name  = " + fullName;
	        
	        try (Connection conn = DriverManager.getConnection(url);
	                Statement stmt = conn.createStatement()) {
	            // create a new table
	            stmt.execute(sql);
	        } catch (SQLException e) {
	            System.out.println(e.getMessage());
	        }
		}
	}
}
