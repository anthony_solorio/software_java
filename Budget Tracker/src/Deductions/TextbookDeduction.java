package Deductions;

//only once at the beginning of the semester
public class TextbookDeduction {
	private float deduct;

	public float getDeduct() {
		return deduct;
	}

	public void setDeduct(float deduct) {
		this.deduct = deduct;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(deduct);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TextbookDeduction other = (TextbookDeduction) obj;
		if (Float.floatToIntBits(deduct) != Float.floatToIntBits(other.deduct))
			return false;
		return true;
	}
}
