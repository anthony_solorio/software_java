package Deductions;

public class UtilityDeduction {
	private float gas = 0, water = 0, electricity = 0, cable = 0, internet = 0, phone = 0;
	private float total =  0;
	
	public void calculateTotalUtility() {
		this.total = getGas() + getWater() + getElectricity() + getCable() + getInternet() + getPhone();
	}
	
	public float getTotal() {
		return total;
	}

	public float getGas() {
		return gas;
	}

	public void setGas(float gas) {
		this.gas = gas;
	}

	public float getWater() {
		return water;
	}

	public void setWater(float water) {
		this.water = water;
	}

	public float getElectricity() {
		return electricity;
	}

	public void setElectricity(float electricity) {
		this.electricity = electricity;
	}

	public float getCable() {
		return cable;
	}

	public void setCable(float cable) {
		this.cable = cable;
	}

	public float getInternet() {
		return internet;
	}

	public void setInternet(float internet) {
		this.internet = internet;
	}

	public float getPhone() {
		return phone;
	}

	public void setPhone(float phone) {
		this.phone = phone;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(cable);
		result = prime * result + Float.floatToIntBits(electricity);
		result = prime * result + Float.floatToIntBits(gas);
		result = prime * result + Float.floatToIntBits(internet);
		result = prime * result + Float.floatToIntBits(phone);
		result = prime * result + Float.floatToIntBits(total);
		result = prime * result + Float.floatToIntBits(water);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UtilityDeduction other = (UtilityDeduction) obj;
		if (Float.floatToIntBits(cable) != Float.floatToIntBits(other.cable))
			return false;
		if (Float.floatToIntBits(electricity) != Float.floatToIntBits(other.electricity))
			return false;
		if (Float.floatToIntBits(gas) != Float.floatToIntBits(other.gas))
			return false;
		if (Float.floatToIntBits(internet) != Float.floatToIntBits(other.internet))
			return false;
		if (Float.floatToIntBits(phone) != Float.floatToIntBits(other.phone))
			return false;
		if (Float.floatToIntBits(total) != Float.floatToIntBits(other.total))
			return false;
		if (Float.floatToIntBits(water) != Float.floatToIntBits(other.water))
			return false;
		return true;
	}

	
}
