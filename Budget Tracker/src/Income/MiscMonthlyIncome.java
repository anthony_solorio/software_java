package Income;

public class MiscMonthlyIncome {
	private float income;

	public float getIncome() {
		return income;
	}

	public void setIncome(float income) {
		this.income = income;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(income);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MiscMonthlyIncome other = (MiscMonthlyIncome) obj;
		if (Float.floatToIntBits(income) != Float.floatToIntBits(other.income))
			return false;
		return true;
	}
}
