package Income;

//this is assuming the loans need to start being paid off
//after college and this student is still in college

public class LoanIncome {
	private float income;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(income);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LoanIncome other = (LoanIncome) obj;
		if (Float.floatToIntBits(income) != Float.floatToIntBits(other.income))
			return false;
		return true;
	}

	public float getIncome() {
		return income;
	}

	public void setIncome(float income) {
		this.income = income;
	}
}
