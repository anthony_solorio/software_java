package StartingLine;

public class Base {
	private float StartingIncome = 0;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(StartingIncome);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Base other = (Base) obj;
		if (Float.floatToIntBits(StartingIncome) != Float.floatToIntBits(other.StartingIncome))
			return false;
		return true;
	}

	public float getStartingIncome() {
		return StartingIncome;
	}

	public void setStartingIncome(float startingIncome) {
		StartingIncome = startingIncome;
	}
}
