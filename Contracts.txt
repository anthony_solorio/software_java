User - Can be returing or new. Saves in costs for living as a student.
Deduct - all deducts store the cost the users enter to later deduct the total deduction
	from the total income. All deducts have a getDeduct and setDeduct which is later saved and retrieved to the database.
Income - all income store how much money the user brings in either monthly or once.
	The total deduction will be taken from the total income at the end. All incomes have a setIncome and getIncome which is later saved 
	and retrieved from the database.
Calculator - This is what the user interacts with. The calculator will guide the user 
	in saving money.
Database - All the information the user enters is stored and updated here.

How all these work togeter can be seen in DomainModel.pdf and the other UML Diagrams.